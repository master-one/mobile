package com.projet.application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import com.projet.application.model.LoginInfo;
import com.projet.application.model.NotificationInfo;
import com.projet.application.services.DataRequestService;
import com.projet.application.services.MessagingService;
import com.projet.application.utils.Use;
import com.projet.application.utils.ViewUtil;


public class SplashScreenActivity extends Activity {

    private void goToLogin() {
        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToMain() {
        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        LoginInfo loginInfo = ViewUtil.getLoginInfo(getApplicationContext());

        if (loginInfo != null && loginInfo.isSuccess()) {
            goToMain();
        } else {
            MessagingService.setTokenInfo(this);
            new DataLoading().execute(ViewUtil.readSession(getApplicationContext(), Use.TOKEN_KEY));
        }
    }

    @SuppressLint("StaticFieldLeak")
    class DataLoading extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... values) {
            DataRequestService.sendNotification(getApplicationContext(), values[0], new NotificationInfo(
                    "Bienvenu dans video player", "Une application pour regarder des video YouTube"
            ));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            goToLogin();
        }
    }
}
