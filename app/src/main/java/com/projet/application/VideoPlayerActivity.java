package com.projet.application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.projet.application.adapter.ListRecyclerAdapter;
import com.projet.application.model.NotificationInfo;
import com.projet.application.model.VideoInfo;
import com.projet.application.services.DataRequestService;
import com.projet.application.utils.Use;
import com.projet.application.utils.ViewUtil;

import java.util.ArrayList;

public class VideoPlayerActivity extends Activity {

    private void setListVideo(ArrayList<VideoInfo> videos) {
        RecyclerView rv = findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(new ListRecyclerAdapter(R.layout.video_item_detail,this, videos));
    }

    private void setVideoPlayerFragment() {
        final String videoId = getIntent().getStringExtra(Use.VIDEO_ID);
        final String videoInfo = getIntent().getStringExtra(Use.VIDEO_INFO);

        YouTubePlayerFragment youtubeFragment = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.video_fragment);
        youtubeFragment.initialize(Use.API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.cueVideo(videoId);
                String token = ViewUtil.readSession(getApplicationContext(), Use.TOKEN_KEY);
                new SendNotification(Use.gson.fromJson(videoInfo, VideoInfo.class)).execute(token);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                System.out.println("Erreur à traiter");
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player);
        setVideoPlayerFragment();
        LoadVideos videos = new LoadVideos();
        videos.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class LoadVideos extends AsyncTask<Void, Void, ArrayList<VideoInfo>> {
        protected ArrayList<VideoInfo> doInBackground(Void... urls) {
            return DataRequestService.getVideosInfo(getBaseContext());
        }

        protected void onPostExecute(ArrayList<VideoInfo> result) {
            setListVideo(result != null ? result : new ArrayList<VideoInfo>());
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class SendNotification extends AsyncTask<String, Void, Void> {
        private VideoInfo videoInfo;

        SendNotification(VideoInfo videoInfo) {
            this.videoInfo = videoInfo;
        }

        @Override
        protected Void doInBackground(String... values) {
            DataRequestService.sendNotification(getApplicationContext(), values[0], new NotificationInfo(
                    videoInfo.getTitle(), String.format("Vous êtes entraine d'écouter %s", videoInfo.getTitle())
            ));
            return null;
        }
    }
}
