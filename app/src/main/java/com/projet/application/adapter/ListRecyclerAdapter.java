package com.projet.application.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.projet.application.R;
import com.projet.application.VideoPlayerActivity;
import com.projet.application.model.VideoInfo;
import com.projet.application.utils.Use;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListRecyclerAdapter extends RecyclerView.Adapter<ListRecyclerAdapter.VideoViewHolder> {

    private final Context              context;
    private final int                  ressourceItem;
    private final ArrayList<VideoInfo> videoInfos;

    public ListRecyclerAdapter(int ressourceItem, Context context, ArrayList<VideoInfo> providers) {
        this.videoInfos = providers;
        this.context = context;
        this.ressourceItem = ressourceItem;
    }

    private void setVideoThumbnail(final String youtId, final ImageView imageView) {
        String imageUrl = "https://i.ytimg.com/vi/" + youtId + "/hqdefault.jpg";
        Picasso.get().load(imageUrl).into(imageView);
    }

    @NonNull
    @Override
    public VideoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(ressourceItem, viewGroup, false);
        return new VideoViewHolder(context, v);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoViewHolder viewHolder, int i) {
        viewHolder.setVideoId(videoInfos.get(i).getIdyoutube());
        viewHolder.videoName.setText(videoInfos.get(i).getTitle());
        viewHolder.videoSubName.setText("");
        viewHolder.setVideoInfo(videoInfos.get(i));
        setVideoThumbnail(videoInfos.get(i).getIdyoutube(), viewHolder.videoImage);
    }

    @Override
    public int getItemCount() {
        return videoInfos.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    static class VideoViewHolder extends RecyclerView.ViewHolder {
        VideoInfo videoInfo;
        CardView  cv;
        TextView  videoName;
        TextView  videoSubName;
        ImageView videoImage;
        String    videoId;

        void setVideoId(String videoId) {
            this.videoId = videoId;
        }

        void setVideoInfo(VideoInfo videoInfo) {
            this.videoInfo = videoInfo;
        }

        VideoViewHolder(final Context context, View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            videoName = itemView.findViewById(R.id.video_name);
            videoSubName = itemView.findViewById(R.id.video_sub_name);
            videoImage = itemView.findViewById(R.id.video_image_id);

            cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, VideoPlayerActivity.class);
                    intent.putExtra(Use.VIDEO_ID, videoId);
                    intent.putExtra(Use.VIDEO_INFO, Use.gson.toJson(videoInfo));
                    context.startActivity(intent);
                }
            });
        }
    }
}
