package com.projet.application;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.projet.application.model.LoginInfo;
import com.projet.application.services.DataRequestService;
import com.projet.application.utils.Use;
import com.projet.application.utils.ViewUtil;

public class LoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void verifierLoginInfo(View view) {
        EditText   username   = findViewById(R.id.username);
        EditText   password   = findViewById(R.id.password);
        LoginAsync loginAsync = new LoginAsync();
        loginAsync.execute(username.getEditableText().toString(), password.getEditableText().toString());
    }

    private void saveSession(LoginInfo info) {
        ViewUtil.saveSession(getApplicationContext(), Use.LOGIN_INFO, Use.gson.toJson(info));
    }

    private void goToMain() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @SuppressLint("StaticFieldLeak")
    class LoginAsync extends AsyncTask<String, Void, LoginInfo> {
        @Override
        protected LoginInfo doInBackground(String... logins) {
            return DataRequestService.login(logins[0], logins[1], getBaseContext());
        }

        @Override
        protected void onPostExecute(LoginInfo info) {
            if (info.isSuccess()) {
                saveSession(info);
                goToMain();
            }
        }
    }
}
