package com.projet.application.services;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.projet.application.utils.Use;
import com.projet.application.utils.ViewUtil;

import java.util.Objects;

public class MessagingService {

    public static void setTokenInfo(final Context context) {
        FirebaseApp.initializeApp(context);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    Log.w("INSTANCE_ID", "getInstanceId failed", task.getException());
                    return;
                }
                String token = Objects.requireNonNull(task.getResult()).getToken();
                ViewUtil.saveSession(context, Use.TOKEN_KEY, token);
            }
        });
    }
}
