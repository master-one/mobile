package com.projet.application.services.utils;

import android.content.Context;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.projet.application.implementation.IAppRequest;
import com.projet.application.utils.Use;

import java.util.ArrayList;
import java.util.Map;

public class DataRequest<T> implements IAppRequest<T> {

    public static String url(String path) {
        return String.format("%s/%s", Use.BASE_URL, path);
    }

    @Override
    public ArrayList<T> getDataCollection(Context context, int method, String uri, String body, Map<String, String> params, Map<String, String> headers) throws InterruptedException {
        final VolleyError[] volleyError = new VolleyError[1];
        RequestCollection<T> request = new RequestCollection<>(method, url(uri), headers, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyError[0] = error;
            }
        });
        request.setParams(params);
        request.setBody(body);
        Volley.newRequestQueue(context).add(request);

        while (true) {
            if (request.getResult() != null)
                return request.getResult();
            if(volleyError[0] != null) {
                return new ArrayList<>();
            }
            Thread.sleep(500);
        }
    }

    @Override
    public T getDataObject(Context context, int method, String uri, Class<T> cl, String body, Map<String, String> params, Map<String, String> headers) throws InterruptedException {
        final VolleyError[] volleyError = new VolleyError[1];
        RequestObject<T> request = new RequestObject<>(method,url(uri), cl, headers, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyError[0] = error;
            }
        });
        request.setParams(params);
        request.setBody(body);
        Volley.newRequestQueue(context).add(request);

        while (true) {
            if (request.getReslut() != null)
                return request.getReslut();
            if(volleyError[0] != null) {
                System.out.println(volleyError[0].getMessage());
                return null;
            }
            Thread.sleep(500);
        }
    }
}
