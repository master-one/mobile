package com.projet.application.services.utils;

import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class RequestObject<T> extends Request<T> {
    private       T                   reslut;
    private final Gson                gson = new Gson();
    private final Class<T>            clazz;
    private final Map<String, String> headers;
    private       String              body;
    private       Map<String, String> params;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url     URL of the request to make
     * @param clazz   Relevant class object, for Gson's reflection
     * @param headers Map of request headers
     */
    RequestObject(int method, final String url, Class<T> clazz, Map<String, String> headers, Response.ErrorListener listener) {
        super(method, url, listener);
        this.clazz = clazz;
        this.headers = headers;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    @Override
    public byte[] getBody() {
        return body == null ? null : body.getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public Map<String, String> getParams() throws AuthFailureError {
        return params != null ? params : super.getParams();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected void deliverResponse(T response) {
        this.reslut = response;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            System.out.println(json);
            return Response.success(gson.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    public T getReslut() {
        return this.reslut;
    }
}
