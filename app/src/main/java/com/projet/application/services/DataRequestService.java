package com.projet.application.services;

import android.content.Context;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.projet.application.implementation.IAppRequest;
import com.projet.application.model.LoginInfo;
import com.projet.application.model.NotificationInfo;
import com.projet.application.model.NotificationProvider;
import com.projet.application.model.VideoInfo;
import com.projet.application.services.utils.DataRequest;
import com.projet.application.utils.PathInfo;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class DataRequestService {
    public static ArrayList<VideoInfo> getVideos(Context context) {
        try {
            IAppRequest<VideoInfo> request = new DataRequest<>();
            return request.getDataCollection(context, Request.Method.GET, PathInfo.VIDOES, null, null, null);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public static NotificationProvider sendNotification(Context context, String token, NotificationInfo notificationInfo) {
        try {
            JSONObject jsonInfo = new JSONObject();
            jsonInfo.put("title", notificationInfo.getTitle());
            jsonInfo.put("description", notificationInfo.getDescription());
            jsonInfo.put("channel_id", String.valueOf(Calendar.getInstance().getTime()));
            jsonInfo.put("token", token);
            String body = jsonInfo.toString();
            System.out.println(body);
            Map<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "Application/json");

            IAppRequest<NotificationProvider> request = new DataRequest<>();
            return request.getDataObject(context, Request.Method.POST, PathInfo.USERS_NOTIF, NotificationProvider.class, body, null, headers);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new NotificationProvider();
    }

    public static LoginInfo login(String username, String password, Context context) {
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("username", username);
            jsonBody.put("password", password);
            final String body = jsonBody.toString();

            Map<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "Application/json");

            IAppRequest<LoginInfo> request = new DataRequest<>();
            return request.getDataObject(
                    context,
                    Request.Method.POST,
                    PathInfo.USERS_AUTH,
                    LoginInfo.class,
                    body,
                    null,
                    headers);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new LoginInfo();
    }

    public static ArrayList<VideoInfo> getVideosInfo(Context context) {
        try {
            RequestQueue                 queue       = Volley.newRequestQueue(context);
            String                       url         = DataRequest.url(PathInfo.VIDOES);
            final ArrayList<VideoInfo>[] videos      = new ArrayList[1];
            final VolleyError[]          volleyError = new VolleyError[1];

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Gson gson = new Gson();
                    videos[0] = gson.fromJson(response, new TypeToken<ArrayList<VideoInfo>>() {
                    }.getType());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    volleyError[0] = error;
                }
            });

            queue.add(stringRequest);

            while (true) {
                if (videos[0] != null)
                    return videos[0];
                if (volleyError[0] != null) {
                    System.out.println(volleyError[0].getMessage());
                }
                Thread.sleep(500);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return new ArrayList<>();
    }
}
