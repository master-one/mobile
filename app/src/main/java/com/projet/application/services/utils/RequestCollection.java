package com.projet.application.services.utils;

import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Map;

public class RequestCollection<T> extends Request<ArrayList<T>> {
    private       ArrayList<T>        result;
    private final Gson                gson = new Gson();
    private final Map<String, String> headers;
    private       Map<String, String> params;
    private       String              body;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url     URL of the request to make
     * @param headers headers of request mapping
     */
    public RequestCollection(int method, final String url, Map<String, String> headers, Response.ErrorListener listener) {
        super(method, url, listener);

        this.headers = headers;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    public Map<String, String> getParams() throws AuthFailureError {
        return params != null ? params : super.getParams();
    }

    @Override
    public byte[] getBody() {
        return body == null ? null : body.getBytes(StandardCharsets.UTF_8);
    }

    @Override
    protected Response<ArrayList<T>> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Type type = new TypeToken<ArrayList<T>>() {
            }.getType();
            result = gson.fromJson(json, type);
            return Response.success(result, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(ArrayList<T> response) {
        this.result = response;
        System.out.println(getUrl());
    }

    public ArrayList<T> getResult() {
        return this.result;
    }
}
