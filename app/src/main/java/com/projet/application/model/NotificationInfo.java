package com.projet.application.model;

public class NotificationInfo {
    private String title;
    private String description;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public NotificationInfo(String title, String description) {
        this.title = title;
        this.description = description;
    }
}
