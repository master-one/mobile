package com.projet.application.model;

public class VideoInfoProvider {

    private final String videoName;
    private final String videoSubName;
    private final int imageId;
    private String videoId = "DDw9y4mQBVo";


    public VideoInfoProvider(String videoName, String videoSubName, int imageId) {
        this.videoName = videoName;
        this.videoSubName = videoSubName;
        this.imageId = imageId;
    }

    public String getVideoSubName() {
        return videoSubName;
    }

    public String getVideoName() {
        return videoName;
    }

    public int getImageId() {
        return imageId;
    }

    public String getVideoId() {
        return videoId;
    }
}
