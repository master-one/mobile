package com.projet.application.model;

public class NotificationProvider {
    private String message;
    private String success;

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
