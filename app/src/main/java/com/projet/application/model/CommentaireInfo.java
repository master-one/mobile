package com.projet.application.model;

class CommentaireInfo {
    private String _id;
    private String username;
    private String text;

    public String get_id() {
        return _id;
    }

    public String getUsername() {
        return username;
    }

    public String getText() {
        return text;
    }
}
