package com.projet.application.model;

public class LoginInfo {
    private boolean  success;
    private String   token;
    private String   msg;
    private UserInfo user;

    public boolean isSuccess() {
        return success;
    }

    public String getToken() {
        return token;
    }

    public UserInfo getUser() {
        return user;
    }
}
