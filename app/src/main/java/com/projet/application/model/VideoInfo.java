package com.projet.application.model;

public class VideoInfo {
    private String _id;
    private String title;
    private String idyoutube;
    private CommentaireInfo[] commentaires;

    public String get_id() {
        return _id;
    }

    public String getTitle() {
        return title;
    }

    public String getIdyoutube() {
        return idyoutube;
    }

    public CommentaireInfo[] getCommentaires() {
        return commentaires;
    }
}
