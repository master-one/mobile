package com.projet.application.model;

public class UserInfo {
    private String id;
    private String name;
    private String username;
    private String email;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }
}
