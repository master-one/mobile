package com.projet.application.utils;

import com.google.gson.Gson;

public class Use {
    public static final Gson gson = new Gson();

    public static final String API_KEY = "AIzaSyBEi05PUnux4rYVXAd1qrjVPwsk7QVCfwU";

    public static final String VIDEO_ID = "VIDEO_ID";

    public static final String DEFAULT_VIDEO_ID = "DDw9y4mQBVo";

//    public static final String BASE_URL = "http://localhost:8080";

    public static final String BASE_URL = "https://m1p6mean-tolotravalery.herokuapp.com";

    public static final String KEY_SESS_INFO = "KEY_SESS_INFO";

    public static final String LOGIN_INFO = "LOGIN_INFO";

    public static final String TOPIC = "VideoInfo";

    public static final String TOKEN_KEY = "TOKEN_KEY";

    public static final String VIDEO_INFO = "VIDEO_INFO";

}
