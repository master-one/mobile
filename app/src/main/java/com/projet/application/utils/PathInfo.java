package com.projet.application.utils;

public abstract class PathInfo {
    public final static String VIDOES      = "videos/videos";
    public final static String USERS_AUTH  = "users/authenticate";
    public static final String USERS_NOTIF = "users/notif";
}
