package com.projet.application.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import com.projet.application.model.LoginInfo;

public class ViewUtil {

    public static void showView(View view, final boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public static ProgressDialog progress(Context context) {
        ProgressDialog dialog = ProgressDialog.show(context, "", "Please wait...", true);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    public static ProgressDialog deconnection(Context context) {
        ProgressDialog dialog = ProgressDialog.show(context, "", "Deconnexion encours...", true);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    public static void showLoader(Context context, View view, final View loader, final boolean load) {
        int shortAnimTime = context.getResources().getInteger(android.R.integer.config_shortAnimTime);
        loader.setVisibility(load ? View.VISIBLE : View.GONE);
        loader.animate().setDuration(shortAnimTime).alpha(load ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                loader.setVisibility(load ? View.VISIBLE : View.GONE);
            }
        });
        view.setVisibility(load ? View.GONE : View.VISIBLE);
    }

    public static void showLoaderLogin(View view, final View loader, final boolean load) {
        loader.setVisibility(load ? View.VISIBLE : View.INVISIBLE);
        view.setVisibility(load ? View.INVISIBLE : View.VISIBLE);
    }

    public static void deleteSession(Context context) {
        context.getSharedPreferences(Use.KEY_SESS_INFO, Context.MODE_PRIVATE).edit().clear().apply();
    }

    public static void saveSession(Context context, String key, String val) {
        context.getSharedPreferences(Use.KEY_SESS_INFO, Context.MODE_PRIVATE).edit().putString(key, val).apply();
    }

    public static String readSession(Context context, String key) {
        if (context != null)
            return context.getSharedPreferences(Use.KEY_SESS_INFO, Context.MODE_PRIVATE).getString(key, "");
        return null;
    }

    public static LoginInfo getLoginInfo(Context context) {
        return Use.gson.fromJson(readSession(context, Use.LOGIN_INFO), LoginInfo.class);
    }
}

