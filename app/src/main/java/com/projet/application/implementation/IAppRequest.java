package com.projet.application.implementation;

import android.content.Context;

import java.util.ArrayList;
import java.util.Map;

public interface IAppRequest<T> {

    ArrayList<T> getDataCollection(Context context, int method, String uri, String body, Map<String, String> params, Map<String, String> headers) throws InterruptedException;

    T getDataObject(Context context, int method, String uri, Class<T> cl, String body, Map<String, String> params, Map<String, String> headers) throws InterruptedException;
}
